package com.ab.eventmaster;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;



public class DataBase extends SQLiteOpenHelper {


     static final int DATABASE_VERSION = 2;

    // Database Name
    public static final String Database_name = "my_database";

    public static final String Table_Name = "customer_table";

    public static final String Key_Id = "key_id";
    public static final String Customer_name = "name";
    public static final String Customer_address = "address";
    public static final String Customer_mobile = "mobile";
    public static final String Customer_doj= "doj";


    public DataBase(Context context ) {
        super(context, Database_name, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String Create_Table_Customer = "CREATE TABLE " + Table_Name + "("+ Key_Id +" INTEGER PRIMARY KEY AUTOINCREMENT , " + Customer_name + " TEXT , "
                                        + Customer_address +" TEXT , " + Customer_mobile + " TEXT , " + Customer_doj + " TEXT )";
        sqLiteDatabase.execSQL(Create_Table_Customer);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS" + Table_Name);
        onCreate(sqLiteDatabase);
    }
}
