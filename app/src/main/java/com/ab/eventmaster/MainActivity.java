package com.ab.eventmaster;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText name_edit,address_edit_1,mobile_edit,doj_edit;
    Button save_button,view_list;


    DataBase dataBase;

//hello

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name_edit = (EditText) findViewById(R.id.name_edit);
        address_edit_1 = (EditText) findViewById(R.id.address_edit_1);
        mobile_edit = (EditText) findViewById(R.id.mobile_edit);
        doj_edit = (EditText) findViewById(R.id.doj_edit);
        save_button =(Button) findViewById(R.id.save_button);
        view_list =(Button)  findViewById(R.id.view_list);


        dataBase =  new DataBase(getApplicationContext());

        final Bundle bundle = getIntent().getExtras();
        Log.d("bundle", String.valueOf(bundle));

        String id = null;
        if (bundle  != null ){
            id = bundle.getString(DataBase.Key_Id);

            name_edit.setText(bundle.getString(DataBase.Customer_name));
            address_edit_1.setText(bundle.getString(DataBase.Customer_address));
            mobile_edit.setText(bundle.getString(DataBase.Customer_mobile));
            doj_edit.setText(bundle.getString(DataBase.Customer_doj));
            save_button.setText("Update");
            save_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    SQLiteDatabase db = dataBase.getWritableDatabase();
                    ContentValues values = new ContentValues();

                    values.put(DataBase.Customer_name, name_edit.getText().toString());
                    values.put(DataBase.Customer_address,address_edit_1.getText().toString());
                    values.put(DataBase.Customer_mobile,mobile_edit.getText().toString());
                    values.put(DataBase.Customer_doj,doj_edit.getText().toString());


                    db.update(DataBase.Table_Name, values, DataBase.Key_Id + "= ?", new String[]{bundle.getString(DataBase.Key_Id)});
                    db.close(); // Closing database connection


                }
            });


        }else {

            save_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    SQLiteDatabase db = dataBase.getWritableDatabase();
                    ContentValues values = new ContentValues();

                    values.put(DataBase.Customer_name, name_edit.getText().toString());
                    values.put(DataBase.Customer_address,address_edit_1.getText().toString());
                    values.put(DataBase.Customer_mobile,mobile_edit.getText().toString());
                    values.put(DataBase.Customer_doj,doj_edit.getText().toString());

                    long student_Id = db.insert(DataBase.Table_Name, null, values);
                    db.close(); // Closing database connection
                    Log.d("value", String.valueOf(student_Id));

                }
            });

        }







        view_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (getApplicationContext(), ViewList.class);
                startActivity(intent);
            }
        });


    }
}
