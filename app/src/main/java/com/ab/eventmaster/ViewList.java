package com.ab.eventmaster;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ankush on 2/14/2018.
 */

public class ViewList extends AppCompatActivity {


    ListView list;

    DataBase dataBase;

    ViewListAdapter adapter;

    Button add;
    Button delete_all;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_list);

        list = (ListView) findViewById(R.id.list);
        add = (Button) findViewById(R.id.add);
        delete_all = (Button) findViewById(R.id.delete_all);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplication(), MainActivity.class);
                startActivity(intent);
            }
        });

        delete_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SQLiteDatabase db = dataBase.getWritableDatabase();
                db.execSQL("delete from "+ DataBase.Table_Name);

                Intent intent = new Intent(getApplication(), ViewList.class);
                startActivity(intent);
            }
        });



        dataBase = new DataBase(getApplicationContext());
        SQLiteDatabase db = dataBase.getReadableDatabase();

        ArrayList <HashMap<String,String>> customer_list =  new ArrayList<HashMap<String, String>>();
        String select_query = "SELECT "+ DataBase.Key_Id + " , "+ DataBase.Customer_name + " , " + DataBase.Customer_address +
                                " , " + DataBase.Customer_mobile + " , " + DataBase.Customer_doj + " FROM " + DataBase.Table_Name;

        Cursor cursor = db.rawQuery(select_query,null);

        if (cursor.moveToFirst()){

            do {

                HashMap <String,String> customer = new HashMap<String, String>();
                customer.put(DataBase.Key_Id , cursor.getString(cursor.getColumnIndex(DataBase.Key_Id)));
                customer.put(DataBase.Customer_name , cursor.getString(cursor.getColumnIndex(DataBase.Customer_name)));
                customer.put(DataBase.Customer_address , cursor.getString(cursor.getColumnIndex(DataBase.Customer_address)));
                customer.put(DataBase.Customer_mobile , cursor.getString(cursor.getColumnIndex(DataBase.Customer_mobile)));
                customer.put(DataBase.Customer_doj , cursor.getString(cursor.getColumnIndex(DataBase.Customer_doj)));
                customer_list.add(customer);
            }while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        Log.d("all_data" , String.valueOf(customer_list));

        adapter = new ViewListAdapter(getApplicationContext(),customer_list);
        list.setAdapter(adapter);

    }
}
