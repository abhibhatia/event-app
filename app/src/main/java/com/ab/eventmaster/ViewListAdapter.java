package com.ab.eventmaster;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.zip.Inflater;


public class ViewListAdapter extends BaseAdapter {

    Context context;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> result_tp = new HashMap<String, String>();


    DataBase dataBase;


    public ViewListAdapter(Context context, ArrayList<HashMap<String, String>> customer_list) {
        this.context = context;
        this.data = customer_list;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View item_view = inflater.inflate(R.layout.view_list_adapter, viewGroup, false);


        dataBase = new DataBase(context);

        TextView name =  (TextView) item_view.findViewById(R.id.name);
        TextView address =  (TextView) item_view.findViewById(R.id.address);
        TextView mobile =  (TextView) item_view.findViewById(R.id.mobile);
        TextView doj =  (TextView) item_view.findViewById(R.id.doj);

        Button edit = (Button) item_view.findViewById(R.id.edit);
        Button delete = (Button) item_view.findViewById(R.id.delete);

        result_tp = data.get(i);

        name.setText(result_tp.get(DataBase.Customer_name));
        address.setText(result_tp.get(DataBase.Customer_address));
        mobile.setText(result_tp.get(DataBase.Customer_mobile));
        doj.setText(result_tp.get(DataBase.Customer_doj));

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                result_tp = data.get(i);
                dataBase = new DataBase(context);
                SQLiteDatabase db = dataBase.getReadableDatabase();
                //ArrayList <HashMap<String,String>> customer_list =  new ArrayList<HashMap<String, String>>();

                String select_query = "SELECT "+ DataBase.Key_Id + " , "+ DataBase.Customer_name + " , " + DataBase.Customer_address +
                        " , " + DataBase.Customer_mobile + " , " + DataBase.Customer_doj + " FROM " + DataBase.Table_Name + " WHERE "
                       + DataBase.Key_Id + "=?" ;

                Cursor cursor = db.rawQuery(select_query, new String[]{result_tp.get(DataBase.Key_Id)});
                Bundle b = new Bundle();
                if (cursor.moveToFirst()){

                    do {
                       // HashMap <String,String> customer = new HashMap<String, String>();
                        b.putString(DataBase.Key_Id , cursor.getString(cursor.getColumnIndex(DataBase.Key_Id)));
                        b.putString(DataBase.Customer_name , cursor.getString(cursor.getColumnIndex(DataBase.Customer_name)));
                        b.putString(DataBase.Customer_address , cursor.getString(cursor.getColumnIndex(DataBase.Customer_address)));
                        b.putString(DataBase.Customer_mobile , cursor.getString(cursor.getColumnIndex(DataBase.Customer_mobile)));
                        b.putString(DataBase.Customer_doj , cursor.getString(cursor.getColumnIndex(DataBase.Customer_doj)));
                       // customer_list.add(customer);
                    }while (cursor.moveToNext());
                }
                cursor.close();
                db.close();

                Log.d("all_data" , String.valueOf(b));
                Intent intent = new Intent(context,MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtras(b);
                context.startActivity(intent);

            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                result_tp = data.get(i);
                Log.d("id",result_tp.get(DataBase.Key_Id));

                SQLiteDatabase db = dataBase.getWritableDatabase();
                db.delete(DataBase.Table_Name , DataBase.Key_Id +  "= ?" , new String[]{result_tp.get(DataBase.Key_Id)});
                //db.delete(DataBase.Table_Name , DataBase.Key_Id +  "= " + result_tp.get(DataBase.Key_Id));

                Intent intent = new Intent(context, ViewList.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
        });

        return item_view;
    }
}
